#!/bin/bash

cd src

rm ../package.zip
zip -r ../package.zip ./*

cd ..

aws lambda update-function-code \
--region us-west-2 \
--function-name InsightsExcel \
--zip-file fileb://./package.zip \
--publish
