var Excel = require('exceljs');
var _ = require('underscore');

module.exports = {
    createFromSingleJson: function(json_object) {

        var insights = _.pick(json_object, ["seedfile_id", "seedfile_name"]);
        var baseline = _.pick(json_object, ["baseline_id", "baseline_name"]);
        var insights_data = _.omit(json_object, ["seedfile_id", "baseline_id", "seedfile_name", "baseline_name"]);
        var workbook = new Excel.Workbook();
        var row = 1;
        var dollar_items = ["Online Offline Purchase", "Purchase Category", "Credit Line Balances", "Mortgage Balance"]
        workbook.addWorksheet("Summary");

        var worksheet = workbook.getWorksheet("Summary");

        worksheet.addRow(['Sample File', insights.seedfile_name]);
        worksheet.addRow(['Baseline', baseline.baseline_name]);
        console.log(baseline);
        worksheet.getRow(4).font = {
            bold: true
        };
        formatSheet("Summary");
        var ordered_sheet = ["Demographics", "Financial", "Automotive", "Employment", "Political", "Purchase", "Interests"];
        //create worksheet to get the order we want
        for (var i = 0; i < ordered_sheet.length; i++) {
            createWorksheets(ordered_sheet[i]);
        }

        for (var key in insights_data) {
            // createWorksheets(key);
            worksheet = workbook.getWorksheet(key);
            for (var item in insights_data[key]) {
                // console.log(item);
                if (item == "States" || item == "DMAs") {
                    continue;
                }
                if (_.contains(dollar_items, item)) {
                    worksheet.addRow(['Chart Name', 'Value', 'Match ', 'Baseline ', '% Diff']);
                    // console.log(worksheet._rows.length);
                    worksheet.getRow(worksheet._rows.length).font = {
                        bold: true
                    };
                }
                for (var i in insights_data[key][item]) {
                    // console.log(insights_values);
                    insights_values = insights_data[key][item][i];
                    percent = insights_values["seedfile_percent"] || insights_values;
                    baseline_p = insights_values["baseline_percent"] || "";
                    change = insights_values["diff_percent"] || "";
                    worksheet.addRow([item, i, percent, baseline_p, change]);

                }

                worksheet.addRow('');
            }

            formatSheet(key);

            row = 1;
        }
        //write to summary in order
        for (var j = 0; j < ordered_sheet.length; j++) {
            writeToSummary(ordered_sheet[j]);
        }
        formatSummarySheet();
        // workbook.xlsx.writeFile("`filename`.xlsx")
        //     .then(function() {
        //         // done
        //     });
        return workbook;

        function formatSummarySheet() {
            var worksheet = workbook.getWorksheet("Summary");
            var e_column = worksheet.getColumn('E');
            e_column.eachCell(function(cell, rowNumber) {
                if (!isNaN(cell.value)) {
                    if (cell.value > 0) {
                        cell.font = {
                            color: {
                                argb: 'FF009900'
                            },
                            bold: true
                        };
                    } else {
                        cell.font = {
                            color: {
                                argb: 'FFFF0000'
                            }
                        };
                    }
                }
            });
            var b_column = worksheet.getColumn('B');
            b_column.eachCell(function(cell, rowNumber) {
                cellC = 'C' + rowNumber;
                cellC = worksheet.getCell(cellC);

                if (!isNaN(cell.value) && cellC.value == null) {
                    if (cell.value > 0) {
                        cell.font = {
                            color: {
                                argb: 'FF009900'
                            },
                            bold: true
                        };
                    } else {
                        cell.font = {
                            color: {
                                argb: 'FFFF0000'
                            }
                        };
                    }
                }
            });

        }

        function createWorksheets(key) {
            //create sheet for each key
            var worksheet = workbook.addWorksheet(key);
            worksheet.addRow(['Sample File', insights.seedfile_name]);
            row++;
            worksheet.addRow(['Baseline', baseline.baseline_name]);
            row++;
            worksheet.addRow('');
            row++;
            worksheet.addRow(['Chart Name', 'Value', 'Sample %', 'Baseline %', '% Diff']);
            row++;
            worksheet.getRow(4).font = {
                bold: true
            };

        }

        function formatSheet(key) {
            var worksheet = workbook.getWorksheet(key);
            if (key == "Business") {
                worksheet = workbook.getWorksheet("Employment");

            }
            worksheet.getColumn('A').font = {
                bold: true
            };
            worksheet.getColumn('A').width = 20;
            worksheet.getColumn('B').width = 20;
            worksheet.getColumn('C').width = 10;
            worksheet.getColumn('D').width = 10;
            worksheet.getColumn('E').width = 10;
            worksheet.getColumn('F').width = 10;
            var e_column = worksheet.getColumn('E');
            e_column.eachCell(function(cell, rowNumber) {
                if (!isNaN(cell.value)) {
                    if (cell.value > 0) {
                        cell.font = {
                            color: {
                                argb: 'FF009900'
                            },
                            bold: true
                        };
                    } else {
                        cell.font = {
                            color: {
                                argb: 'FFFF0000'
                            }
                        };
                    }
                }
            });
            var b_column = worksheet.getColumn('B');
            b_column.eachCell(function(cell, rowNumber) {
                cellC = 'C' + rowNumber;
                cellC = worksheet.getCell(cellC);

                if (!isNaN(cell.value) && cellC.value == null) {
                    if (cell.value > 0) {
                        cell.font = {
                            color: {
                                argb: 'FF009900'
                            },
                            bold: true
                        };
                    } else {
                        cell.font = {
                            color: {
                                argb: 'FFFF0000'
                            }
                        };
                    }
                }
            });
        }

        function writeToSummary(key) {
            var worksheet = workbook.getWorksheet(key);
            var worksheet_summary = workbook.getWorksheet("Summary");

            if (key == "Business") {
                worksheet = workbook.getWorksheet("Employment");

            }
            if (key == "Business") {
                key = "Employment";
            }
            worksheet_summary.addRow([key]);
            var current_summary_row = worksheet_summary.lastRow;
            // console.log(row1);
            var cellA = current_summary_row.getCell(1);

            cellA.font = {
                color: {
                    argb: 'FFFFFFFF'
                },
                size: '14'
            // bold: true
            };
            cellA.fill = {
                type: 'pattern',
                pattern: 'solid',

                fgColor: {
                    // argb: 'D55015'
                    argb: '003F5F'
                },
                bgColor: {
                    argb: '00000000'
                }
            };

            worksheet_summary.addRow('');

            worksheet.eachRow({
                includeEmpty: true
            }, function(row, rowNumber) {
                if (rowNumber > 3)
                    worksheet_summary.addRow(row.values);

            });

        }
    }
};
