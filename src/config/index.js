var fs = require('fs'),
    path = require('path'),
    nconf = require('nconf');

var configFileName = path.join(__dirname, (process.env.NODE_ENV ? process.env.NODE_ENV : 'default') + '.json');

nconf.argv()
    .env()
    .file({
        file: configFileName
    })
    .defaults({});

module.exports = nconf;
