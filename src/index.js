var config = require('./config');
var excelFromjson = require('./excelfile.js');

exports.handler = function(event, context, callback) {
    console.log("Reading options from event:\n");
    console.log(event);
    // var input_json = event;
    var filename = event.filename;
    var customer_id = event.customer_id;
    var seed_file_id = event.seed_file_id;
    var baseline_file_id = event.baseline_file_id || 'US_Internet_Population';

    var AWS = require('aws-sdk');
    AWS.config.update({
        region: 'us-west-2'
    });
    var lambda = new AWS.Lambda()

    var post_data = {
        "customer_id": customer_id,
        "seed_file_id": seed_file_id,
        "baseline_id": baseline_file_id,
    // "is_from_TAP": true
    }
    var lambda_params = {
        FunctionName: config.get('insights-json-lambda'),
        InvocationType: 'RequestResponse',
        LogType: 'None',
        Payload: JSON.stringify(post_data)
    };
    console.log(lambda_params);
    lambda.invoke(lambda_params, function(err, resp) {
        // console.log("lambda", resp, err);
        if (err) {
            console.log(err);
        }

        var lambda_resp = JSON.parse(resp.Payload)
        console.log(lambda_resp);
        var workbook = excelFromjson.createFromSingleJson(lambda_resp);

        var s3Stream = require('s3-upload-stream')(new AWS.S3())
        var upload = s3Stream.upload({
            "Bucket": config.get('aws_settings').uploads.bucket, //bucket will need to be changed for test vs prod (env var)
            "Key": "insights-excel-files/" + filename + ".xlsx" //filename should be something random
        });
        upload.on('part', function(details) {
            console.log(details);
        });
        upload.on('uploaded', function(details) {
            console.log(details);
            return callback(null, details)

        });

        workbook.xlsx.write(upload);

    });
}
// getInsightsFromLambda();

// var s3Stream = require('s3-upload-stream')(new AWS.S3())
// var upload = s3Stream.upload({
//     "Bucket": config.get('aws_settings').uploads.bucket, //bucket will need to be changed for test vs prod (env var)
//     "Key": "insights-excel-files/" + filename + ".xlsx" //filename should be something random
// });
// upload.on('part', function(details) {
//     console.log(details);
// });
// upload.on('uploaded', function(details) {
//     console.log(details);
//     return callback(null, details)
//
// });
//
// workbook.xlsx.write(upload);
