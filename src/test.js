var excelFromjson = require('./excelfile.js');
var config = require('./config');
var customer_id = 111;
var seed_file_id = 450;
var baseline_file_id = 450; //'US_Internet_Population';
var filename = config.get('aws_settings').uploads.bucket + "test_file" + customer_id;
var AWS = require('aws-sdk');
AWS.config.update({
    region: 'us-west-2'
});
var lambda = new AWS.Lambda()
function getInsightsFromLambda() {

    var post_data = {
        "customer_id": customer_id,
        "seed_file_id": seed_file_id,
        "baseline_id": baseline_file_id,
    // "is_from_TAP": true
    }
    var lambda_params = {
        FunctionName: config.get('insights-json-lambda'),
        InvocationType: 'RequestResponse',
        LogType: 'None',
        Payload: JSON.stringify(post_data)
    };

    lambda.invoke(lambda_params, function(err, resp) {
        // console.log("lambda", resp, err);
        if (err) {
            console.log(err);
        }

        var lambda_resp = JSON.parse(resp.Payload)
        var workbook = excelFromjson.createFromSingleJson(lambda_resp);
        var s3Stream = require('s3-upload-stream')(new AWS.S3())
        var upload = s3Stream.upload({
            "Bucket": config.get('aws_settings').uploads.bucket, //bucket will need to be changed for test vs prod (env var)
            "Key": "insights-excel-files/" + filename + ".xlsx" //filename should be something random
        });
        upload.on('part', function(details) {
            console.log(details);
        });
        upload.on('uploaded', function(details) {
            console.log(details);
            // return callback(null, details)

        });
        workbook.xlsx.writeFile("filename.xlsx")
            .then(function() {
                // done
            });
        workbook.xlsx.write(upload);

    });
}
getInsightsFromLambda();
